package Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@CucumberOptions(features="src/test/java/feature/createleadwithallkeywords.feature",glue= {"steps","pagesCJ"},monochrome=true,snippets = SnippetType.CAMELCASE)

@RunWith(Cucumber.class)

public class RunSteps {
	
	
	

}


//monochrome =true ->Ensures all the conditions in the feature file is covered
//dryrun= true ->Verifies whether all fine without executing testcase
//snippets =>feature file wordings (small instead CAPS) and wordings mentioned in POM annotation are not same this will help to ignore only incase of small to CAPS ot CAPS)