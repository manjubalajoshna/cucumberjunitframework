Feature: Edit a Lead in LeafTap Application

Scenario Outline: Edit Lead
And enter username as DemoSalesManager
And enter password as crmsfa
And click login button 
And click CRMSFA link 
And click Leads button
And click FindLeads button 
And enter firstnameinEditlead as <fName>   
And click FindLeadsbutton 
And clickFirstRow 
And clickEdit
And enterCompanyName as <cName>
When clickUpdateCname
Then verifyExactText as <changedName> 
Examples:
|fName|cName|changedName|
|s|Amazon|Amazon|
|s|Altimetrics|Altimetrics|