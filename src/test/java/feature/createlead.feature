Feature: Create a Lead in LeafTap Application
Scenario: Lead creation with mandatory fields
Given launch the browser
And maximise the browser
And set the timeouts
And enter the URL
And enter the username as DemoSalesManager
And enter the password as crmsfa
And click on login button
And click on CRMSFA link
And click on createleadMenu button
And enter the companyname as ABC
And enter the firstname as Joshita
And enter the lastname as J
When clicks on createlead button
Then close the browser
