package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {
	
	public static ChromeDriver driver; 
	
	@Given("launch the browser")
	public void launchbrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver = new ChromeDriver();
		}
	
	@And("maximise the browser")
	public void maximisebrowser() {
				driver.manage().window().maximize();
	}
	
	@And("set the timeouts")
	public void timeouts() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}
	@And("enter the URL")
	public void URL() {
		driver.get("http://leaftaps.com/opentaps/control/main");
}
	@And("enter the username as (.*)")
	public void enterUserName(String uName) {
		driver.findElementById("username").sendKeys(uName);
		
	}
	
	@And("enter the password as (.*)")
	public void enterPassword(String password) {
		driver.findElementById("password").sendKeys(password);
		
	}
	
	@And("click on login button")
	public void clickLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
		
	}
	
	@And("click on CRMSFA link")
	public void clickCRMSFA() {
		driver.findElementByLinkText("CRM/SFA").click();
		
	}
	
	@And("click on createleadMenu button")
	public void clickCreateLeadMenu() {
		driver.findElementByLinkText("Create Lead").click();
		
	}
	
	@And("enter the companyname as (.*)")
	public void enterCompanyName(String cName) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cName,Keys.TAB);
		
	}
	
	@And("enter the firstname as (.*)")
	public void enterFirstName(String fName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fName,Keys.TAB);
		
	}
	
	@And("enter the lastname as (.*)")
	public void enterLastName(String lName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lName,Keys.TAB);
		
	}
	@When("clicks on createlead button")
	public void clickCreateLeadButton() {
		driver.findElementByXPath("//input[@class='smallSubmit']").click();;
		
	}
	
	@Then("close the browser")
	public void closeBrowser() {
		driver.close();
		
	}
	//Then verify newlead created with companyname as ABC
	
	
	
	
	
	
}
