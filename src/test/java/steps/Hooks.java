package steps;


import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethodsCJ;


public class Hooks extends SeMethodsCJ{
	
	@Before //Cucumber Junit Annotations
	
	public void beforeCucumber(Scenario sc) {
		
		startResult();
		//testCaseName = sc.getName();
		//testCaseDescription =sc.getId();
		testCaseName = "CreateLead";
		testCaseDescription ="Creating a Lead";
		category = "Smoke";
		author= "ManjuSindhu";
		
		/*testCaseName = "EditLead";
		testCaseDescription ="Editing a Lead";
		category = "SIT";
		author= "Manju";*/
		startTestCase();
		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
			
	}
	
	@After //Cucumber Junit Annotations
	
public void afterCucumber(Scenario sc) {
		
		closeAllBrowsers();
		stopResult();
			
	}
	
	
	
	

}
