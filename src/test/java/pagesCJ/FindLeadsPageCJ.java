package pagesCJ;



import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import wdMethods.SeMethodsCJ;


public class FindLeadsPageCJ extends SeMethodsCJ{
	
	//Created for Edit
	
	//@And("enter firstnameinEditlead as (.*)")
	public FindLeadsPageCJ typeFirstName(String fName) {
		WebElement eleFirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstName, fName);
		return this;
	}
	
	//@And("click FindLeadsbutton")
	public FindLeadsPageCJ clickFindLeadsbutton() throws InterruptedException {
		WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads);
		Thread.sleep(4000);
		return this;
	}
	
	//@And("clickFirstRow")
	public ViewLeadPageCJ clickFirstRow() {
		WebElement eleFirstRow = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		String data =eleFirstRow.getText();
		click(eleFirstRow);
		return new ViewLeadPageCJ();
	}
	
	//Created for Delete
	
	public FindLeadsPageCJ clickPhone() throws InterruptedException {
		WebElement eleclickPhone = locateElement("xpath", "//span[text()='Phone']");
		click(eleclickPhone);
	   return this;
	}
	
	public FindLeadsPageCJ typePhoneNumber(String PhoneNumber) throws InterruptedException {
		WebElement elePhoneNumber = locateElement("xpath", "//input[@name='phoneNumber']");
		type(elePhoneNumber, PhoneNumber);
		return this;
	}
	
		
	public FindLeadsPageCJ typeLeadid() throws InterruptedException {
		WebElement eleLeadid = locateElement("xpath", "(//div[@class='x-form-element']/input)[13]");
		String data =eleLeadid.getText();
		type(eleLeadid, data);
		return this;
	}
	

	
	//Created for Duplicate
	
	public FindLeadsPageCJ clickemail() throws InterruptedException {
		WebElement eleemail = locateElement("xpath", "//span[text()='Email']");
		click(eleemail);
	   return this;
	}
	
	public FindLeadsPageCJ typeEmail(String eMail) throws InterruptedException {
		WebElement eleEmail = locateElement("xpath", "//input[@name ='emailAddress']");
		type(eleEmail, eMail);
		return this;
	}
	
	//Created for Merge
	
	//public switchToWindow(1)
	
	
	public MergeLeadsPageCJ clickRowOne() {
		WebElement eleRowOne = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleRowOne);
		return new MergeLeadsPageCJ();
	}
	
	public MergeLeadsPageCJ clickRowTwo() {
		WebElement eleRowTwo = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[2]");
		click(eleRowTwo);
		return new MergeLeadsPageCJ();
	}
	
	
}
