package pagesCJ;

import org.openqa.selenium.WebElement;

import wdMethods.SeMethodsCJ;




public class MergeLeadsPageCJ extends SeMethodsCJ {
	
	public FindLeadsPageCJ clickFromLead() throws InterruptedException {
		WebElement eleFromLead = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(eleFromLead);
		Thread.sleep(1000);
		return new FindLeadsPageCJ();
	}
	
	public FindLeadsPageCJ clickToLead() throws InterruptedException {
		WebElement eleToLead = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(eleToLead);
		Thread.sleep(1000);
		return new FindLeadsPageCJ();
	}
	
	public ViewLeadPageCJ clickMergeButton() {
		WebElement eleMergeButton = locateElement("xpath", "//a[text()='Merge']");
		click(eleMergeButton);
		return new ViewLeadPageCJ();
	}
	
	public MergeLeadsPageCJ clickAlert() {
		acceptAlert();
		return this;
	}
}
