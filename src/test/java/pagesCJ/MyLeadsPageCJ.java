package pagesCJ;

import org.openqa.selenium.WebElement;


import cucumber.api.java.en.And;
import pagesCJ.CreateLeadPageCJ;
import wdMethods.SeMethodsCJ;


public class MyLeadsPageCJ extends SeMethodsCJ{

	@And("click createLead button")
	public CreateLeadPageCJ clickCreateLead() {
		WebElement eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		return new CreateLeadPageCJ();
	}
	
	public FindLeadsPageCJ clickFindLeads() {
		WebElement eleFindLeads = locateElement("linktext", "Find Leads");
		click(eleFindLeads);
		return new FindLeadsPageCJ();
	}
	
	//Merge
	
	public MergeLeadsPageCJ clickMergeLeads() {
		WebElement eleMergeLeads = locateElement("linktext", "Merge Leads");
		click(eleMergeLeads);
		return new MergeLeadsPageCJ();
	}
	
}









