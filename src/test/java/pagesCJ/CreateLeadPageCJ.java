package pagesCJ;



import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import pagesCJ.CreateLeadPageCJ;
import wdMethods.SeMethodsCJ;


public class CreateLeadPageCJ extends SeMethodsCJ{

	@And("enter companyname as (.*)")
	public CreateLeadPageCJ typeCompanyName(String cName) {
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		type(eleCompanyName, cName);
		return this;
	}
	
	@And("enter firstname as (.*)")
	public CreateLeadPageCJ typeFirstName	(String fName) {
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		type(eleFirstName, fName);
		return this;
	}
	
	@And("enter lastname as (.*)")
	public CreateLeadPageCJ typeLastName	(String lName) {
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, lName);
		return this;
	}
	
	@When("clicks createlead1 button")
	public CreateLeadPageCJ clickCreateLead() {
		WebElement eleCreateLead= locateElement("class", "smallSubmit");
		click(eleCreateLead);
		return this; 
	}
	
}









