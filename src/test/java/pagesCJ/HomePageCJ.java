package pagesCJ;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import pagesCJ.MyHomePageCJ;
import steps.Hooks;
import wdMethods.SeMethodsCJ;


public class HomePageCJ extends SeMethodsCJ{

	@And("click CRMSFA link")
	public MyHomePageCJ clickCRMSFA() {
		WebElement eleCRMSFA = locateElement("linktext", "CRM/SFA");
		click(eleCRMSFA);
		return new MyHomePageCJ();
	}
	
}









