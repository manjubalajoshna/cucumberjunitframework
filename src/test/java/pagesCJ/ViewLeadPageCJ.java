package pagesCJ;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class ViewLeadPageCJ extends FindLeadsPageCJ {
	
//Edit Lead	

	//@And("clickEdit")
	public OpenTapsCrmPageCJ clickEdit() {
		WebElement eleEditclick = locateElement("linktext", "Edit");
		click( eleEditclick);
		return new OpenTapsCrmPageCJ();
	}

	//@Then("verifyExactText as (.*)")
	public ViewLeadPageCJ verifyExactText(String changedName) {
		
		
		//WebElement verifyedit = locateElement("xpath", "//span[contains(text(),'changedName')]");
		WebElement verifyedit = locateElement("xpath", "//span[text()='Company Name']");
		verifyExactText(verifyedit, changedName);
		
		return this;
	}

//Delete Lead
	
	public MyLeadsPageCJ clickDelete() {
		WebElement eleDeleteclick = locateElement("xpath", "//a[@class='subMenuButtonDangerous']");
		click( eleDeleteclick);
		return new MyLeadsPageCJ();
	}

	//Merge Lead
	
	public FindLeadsPageCJ clickFindLeads() {
		WebElement eleFindLeads = locateElement("linktext", "Find Leads");
		click( eleFindLeads);
		return new FindLeadsPageCJ();
	}
	
	//Duplicate Lead
	
	public DuplicateLeadPageCJ clickDuplicateLead() {
		WebElement eleclickDuplicateLead = locateElement("xpath", "//a[text()='Duplicate Lead']");
		click( eleclickDuplicateLead);
		return new DuplicateLeadPageCJ();
	}
	
	
}
