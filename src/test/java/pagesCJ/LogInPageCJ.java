package pagesCJ;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import wdMethods.SeMethodsCJ;

public class LogInPageCJ extends SeMethodsCJ{

	@And("enter username as (.*)")
	public LogInPageCJ typeUserName(String data) {
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, data);
		return this;
	}
	
	@And("enter password as (.*)")
	public LogInPageCJ typePassword(String data) {
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, data);
		return this;
	}
	
	@And("click login button")
	public HomePageCJ clickLogin() {
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		return new HomePageCJ();
	}
	
}









