package pagesCJ;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class OpenTapsCrmPageCJ extends ViewLeadPageCJ {
	
	public OpenTapsCrmPageCJ() {
		
		PageFactory.initElements(driver, this);
			
	}
	
	@CacheLookup
	@FindBy(id ="updateLeadForm_companyName")
	WebElement eleCompanyName;
	
	//@And("enterCompanyName as (.*)")
	public OpenTapsCrmPageCJ typeCompanyName(String cName) {
		//WebElement eleCompanyName = locateElement("id", "updateLeadForm_companyName");
		eleCompanyName.clear();
		type(eleCompanyName, cName);
		return this;
	}
	
	
	//@When("clickUpdateCname")
	public ViewLeadPageCJ clickUpdateCname() {
		WebElement eleUpdateclick = locateElement("xpath", "//input[@value='Update']");
		click( eleUpdateclick);
		return new ViewLeadPageCJ();
	}

	
	

}
