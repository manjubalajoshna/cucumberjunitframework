package pagesCJ;

import org.openqa.selenium.WebElement;

import wdMethods.SeMethodsCJ;

public class DuplicateLeadPageCJ extends SeMethodsCJ {
	
public DuplicateLeadPageCJ verifyExactText() {	
		WebElement verifyeditduplicate = locateElement("xpath", "//div[text()='Duplicate Lead']");
		verifyExactText(verifyeditduplicate, "Duplicate Lead");	
		return this;
	}

public ViewLeadPageCJ clickCreateLead() {
	WebElement eleclickCreateLead = locateElement("xpath", "//input[@value='Create Lead']");
	click( eleclickCreateLead);
	return new ViewLeadPageCJ();
}

}
