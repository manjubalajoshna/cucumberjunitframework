package pagesCJ;

import org.openqa.selenium.WebElement;


import cucumber.api.java.en.And;
import wdMethods.SeMethodsCJ;


public class MyHomePageCJ extends SeMethodsCJ
{

	@And("click Leads button")
	public MyLeadsPageCJ clickLeads() {
		WebElement eleLeads = locateElement("linktext", "Leads");
		click(eleLeads);
		return new MyLeadsPageCJ();
	}
	
}









